from transformers import AutoTokenizer
import transformers
import torch
import accelerate
from llm import LLM


class mistralLLM(LLM):
    def __init__(self,model,system_prompt=""):

        super().__init__()

        self.model = model

        self.pipeline = transformers.pipeline(
                "text-generation",
                model=model,
                device_map='auto',
                model_kwargs={"torch_dtype": torch.float16},)

        self.system_prompt = system_prompt

    def query(self,input_string,new_tokens_max=512):
        tokenizer = AutoTokenizer.from_pretrained(self.model)
        messages = [{"role": "user", "content": input_string}]
        prompt = self.pipeline.tokenizer.apply_chat_template(messages, tokenize=False, add_generation_prompt=True)
        outputs = self.pipeline(prompt, max_new_tokens=new_tokens_max, do_sample=True,temperature=0.7, top_k=50, top_p=0.95)
        text = outputs[0]['generated_text']
        #print("original text: " + text)
        bgStart = text.find('[INST]')
        bgEnd = text.find('[/INST]')
        bg = text[bgStart + len('[INST]'):bgEnd].strip()
        response = text[bgEnd + len('[/INST]'):].strip()
        return [bg,response]

