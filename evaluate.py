import sys
from mistralAI import mistralLLM
from transformers import pipeline

# LLM used
llmName = "mistralai/mistral-7b-instruct-v0.2"
llmModel = mistralLLM(model=llmName)

if len(sys.argv) != 3:
    print("Incorrect argument: python evaluation.py newLog.txt metricNumber")
    sys.exit(1)

queries = []
newContexts = []
newResponse = []
LLMs = []
embeds = []

newLog = sys.argv[1]
metricNumber = sys.argv[2]

responseTag = False
endTag = "=============================================================================="
response = ""

#Parsing the log file
with open(newLog, 'r') as file:
    for line in file:
        if line.startswith("====Query:"):
            query = line[len("====Query:"):].strip()
            queries.append(query)
        elif line.startswith("====Context:"):
            context = line[len("====Context:"):].strip()
            context = context[context.find("</SYS_PROMPT>") + len('</SYS_PROMPT>'):].strip()
            newContexts.append(context)
        elif line.startswith("====Output:"):
            response = ""
            responseTag = True
            output = line[len("====Output:"):].strip()
            response += output
        elif line.startswith("====LLM:"):
            llm = line[len("====LLM:"):].strip()
            LLMs.append(llm)
        elif line.startswith("====Embedding:"):
            embed = line[len("====Embedding:"):].strip()
            embeds.append(embed)
        else:
            if line.startswith(endTag) and responseTag == True:
                responseTag = False
                newResponse.append(response)
                response = ""
            elif responseTag == True:
                text = line.strip()
                response += text

print(len(newResponse))

#Evaluate each responses
for i in range(len(newResponse)):
    llm_query3 = f"""Evaluate the response from a virtual teaching assistant based on the given query. Consider the following:

                    Query: {queries[i]}
                    Response: {newResponse[i]}

                    Evaluation Criteria:
                    - Relevance: How well does the response address the query?
                    - Completeness: Does the response fully answer the query without leaving out crucial information?
                    - Depth: Is the depth of the response at least equal to or greater than the depth of the query?

                    Evaluation Outcomes:
                    - FAIL: The response lacks sufficient information to answer the query.
                    - PASS: The response covers the necessary information but could be improved.
                    - GOOD: The response effectively and thoroughly answers the query.

                    Give the evaluation outcome at the beginning of your assessment, output only "FAIL", "PASS", or "GOOD". 
                    Your evaluation must be based on the above criteria.
                    Do not give a GOOD if it's not perfect.
                    Do not give a PASS if it doesn't really answer the query.
                    Give FAIL immediately if you see unfinished English sentences.
                """

    relevancePrompt = f"""Evaluate the relevance of the response from a virtual teaching assistant based on the given query:

                        Query: {queries[i]}
                        Response: {newResponse[i]}

                        Relevance Evaluation Requirements:
                        1. Concept Alignment: Does the response accurately include and explain the concepts mentioned in the query?
                        2. Related Concepts: If the response includes concepts not present in the query, are they relevant and connected to the query's topic?
                        3. Follow-up Relevance: If the response contains a follow-up question, is it directly relevant to the original query?
                        4. Contextual Relevance: Are all parts of the response relevant to the query's overall context and purpose?

                        Evaluation Outcomes:
                        - POOR: The response meets none or only one requirement, showing significant deficiencies in relevance.
                        - UNSATISFACTORY: The response meets two requirements but fails to address others, needing improvement in relevance.
                        - PASS: The response meets three requirements, showing acceptable relevance with some room for improvement.
                        - GOOD: The response meets most requirements, showing a high level of relevance.
                        - OUTSTANDING: The response excels in relevance, meeting all evaluation requirements.

                    Begin your response with only one of the labels: POOR, UNSATISFACTORY, PASS, GOOD, or OUTSTANDING, based solely on the Relevance Evaluation Requirements for a focused and precise evaluation.
                    Be strict in your assessment.
                    """

    completenessPrompt = f"""Evaluate the completeness of the response from a virtual teaching assistant based on the given query:

                        Query: {queries[i]}
                        Response: {newResponse[i]}

                        Completeness Evaluation Requirements:
                        1. Sentence Integrity: Are all sentences in the response complete and finished in terms of the English language?
                        2. Query Coverage: Does the response address all parts of the query without omitting any key elements?
                        3. Information Sufficiency: Does the response provide enough information to fully answer the query?
                        4. Clarity and Understandability: Is the response clear and easy to understand, enhancing the overall completeness?

                        Evaluation Outcomes:
                        - POOR: The response meets none or only one requirement, indicating significant gaps in completeness.
                        - UNSATISFACTORY: The response meets two requirements but lacks in other areas, showing a need for more comprehensive coverage.
                        - PASS: The response meets three requirements, demonstrating a basic level of completeness with room for improvement.
                        - GOOD: The response meets most requirements, indicating a high level of completeness.
                        - OUTSTANDING: The response excels by meeting all completeness requirements, providing a thorough and well-rounded answer.

                    Begin your response with only one of the labels: POOR, UNSATISFACTORY, PASS, GOOD, or OUTSTANDING, based solely on the Completeness Evaluation Requirements for a focused and precise evaluation.
                    Be strict in your assessment.
                    """

    DepthPrompt = f"""Evaluate the depth of the response from a virtual teaching assistant based on the given query:

                    Query: {queries[i]}
                    Response: {newResponse[i]}

                    Depth Evaluation Requirements:
                    1. Depth: Does the response go into the depth that is at least as deep as the question?
                    2. Conceptual Understanding: Does the response demonstrate a deep understanding of the underlying concepts or topics in the query?
                    3. Analytical Insight: Does the response provide analytical insights, critical thinking, or problem-solving approaches relevant to the query?
                    4. Comprehensive Scope: Does the response cover the topic extensively, including various perspectives or dimensions when appropriate?
                    5: Bloom's Taxonomy: Does the bloom's classification of the response equal or higher to that of the query?

                    Evaluation Outcomes:
                    - POOR: The response meets none or only one of the depth requirements.
                    - UNSATISFACTORY: The response meets two depth requirements but lacks significant detail or understanding in other areas.
                    - PASS: The response meets three of the depth requirements, showing a moderate level of depth with some areas left unexplored.
                    - GOOD: The response meets most depth requirements, indicating a thorough exploration of the topic.
                    - OUTSTANDING: The response meets all requirements and provides a comprehensive, insightful analysis.

                    Begin your response with only one of the labels: POOR, UNSATISFACTORY, PASS, GOOD, or OUTSTANDING, based solely on the Depth Evaluation Requirements for a focused and precise evaluation.
                    Be strict in your assessment.
                    """

    print("====LLM: " + LLMs[i])
    print("====Embedding: " + embeds[i])
    print("====Query: " + queries[i])
    print("====Response: " + newResponse[i])

    if metricNumber == '1':
        simplifiedOutput = llmModel.query(llm_query3)
        print("====SimplifiedEvaluation: " + simplifiedOutput[1])
    elif metricNumber == '2':
        relevantOutput = llmModel.query(relevancePrompt)
        print("====RelevanceEvaluation: " + relevantOutput[1])
    elif metricNumber == '3':
        completenessOutput = llmModel.query(completenessPrompt)
        print("====CompletenessEvaluation: " + completenessOutput[1])
    elif metricNumber == '4':
        depthOutput = llmModel.query(DepthPrompt)
        print("====DepthEvaluation: " + depthOutput[1])
    print("\n")
