#You need to import the class for some reason...
from cognitive_stack import CognitiveStack
#even though this already is imported in cognitive_stack, you still need to import it here, otherwise you'd need to call it
#cognitive_stack.chromadb
import chromadb
import chroma_utils
from wiki_qa import wiki_QA
#from openAI_llm import openAI_llm
from collections import deque
import shutil
import os
import random
import string
from mistralAI import mistralLLM
from sentence_transformers import SentenceTransformer
from chromadb.utils import embedding_functions
from datetime import datetime
import pytz
from langchain_community.document_loaders import PyPDFLoader


embName = 'all-MiniLM-L6-v2'
ef = embedding_functions.SentenceTransformerEmbeddingFunction(model_name=embName)

embModel = SentenceTransformer(embName)

llmName = 'mistralai/mistral-7b-instruct-v0.2'
llm = mistralLLM(model=llmName)

#llm = stablelmLLM(model=llmName)

def make_project(project_name):
    # Define the base directory where the "projects" folder is located
    base_directory = "/home/j586yang/Mycroft/projects"

    # Create a path for the project folder
    project_directory = os.path.join(base_directory, project_name)

    # Create directories if they don't exist
    directories_to_create = [
        os.path.join(project_directory, "rawpdf"),
        os.path.join(project_directory, "ingested"),
        os.path.join(project_directory, "chroma_db"),
    ]

    for directory in directories_to_create:
        os.makedirs(directory, exist_ok=True)

    print(f"Project '{project_name}' directories created.")

def load_dbs(project_name):
    chroma_client = chromadb.PersistentClient(path=os.path.join("/home/j586yang/Mycroft/projects",project_name))
    #collection_background = chroma_client.get_or_create_collection(name="background")   
    collection_background = chroma_client.get_or_create_collection(name="background",embedding_function=ef)
    collection_ideas = chroma_client.get_or_create_collection(name="ideas")
    
    #print(collection_background.peek())
    return [chroma_client, collection_background, collection_ideas]

def project_exists(project_name):
    # Define the base directory where the "projects" folder is located
    base_directory = "/home/j586yang/Mycroft/projects"

    # Check if the project directory exists
    project_directory = os.path.join(base_directory, project_name)
    return os.path.exists(project_directory)

def move_rawpdf_to_ingested(project_name):
    print("moving PDFs")
    # Define the base directory where the "projects" folder is located
    base_directory = "/home/j586yang/Mycroft/projects"

    # Create paths for the source (rawpdf) and destination (ingested) directoriesi
    source_directory = os.path.join(base_directory, project_name, "rawpdf")
    destination_directory = os.path.join(base_directory, project_name, "ingested")

    # Get a list of all files in the source directory
    files_to_move = os.listdir(source_directory)
   
    print("source directory: " + source_directory)
    print("length: " + str(len(files_to_move))) 

    # Move each file from source to destination
    for file_name in files_to_move:
        print("file!")
        source_file_path = os.path.join(source_directory, file_name)
        destination_file_path = os.path.join(destination_directory, file_name)
        shutil.move(source_file_path, destination_file_path)
        print(f"Moved '{file_name}' to 'ingested' directory.")


def ingest(project_name, collection):
    #ingest the pdfs first
    chroma_utils.ingest_pdfs(os.path.join(os.path.join("/home/j586yang/Mycroft/projects",project_name),"rawpdf"),collection)
    #move them
    move_rawpdf_to_ingested(project_name)

#Chroma requires a title, but for ideas it doesn't make much sense to create a title, so this will randomly generate one
def generate_random_string(length):
    # Define the characters you want to include in the random string
    characters = string.ascii_letters + string.digits  # You can include other characters if needed

    # Generate a random string of the specified length
    random_string = ''.join(random.choice(characters) for _ in range(length))

    return random_string

    
def record_idea(idea_text,collection,idea_title=None):
    if idea_title is None:
        idea_title = generate_random_string(23)
    chroma_utils.add_to_chroma(idea_text,idea_title,collection)

query_prompt = "You are a helpful teaching assistant, but do not provide irrelevant response to the question. You have access to the background information. Be sure to answer truthfully, but you may feel free to give your own ideas as long as you state that you are doing so. If you can answer using the background information, you should prioritize that. If the background information or previous ideas does not have any relevant information, you must say so. You should also prompt the user with a question at the end of your response, to keep the conversation going or probe more deeply. Asking questions is very important. But don't just ask them what they think. Rather, add to the conversation, put in your own point of view. You are collaborating with the user, you aren't just regurgitating information. But do not ask irrelevant questions"
#summary_prompt ="The following is a brief conversation snippet between a helpful assistant and a user. Summarize the key points that were discussed so far."


project = input("What project are we working on today? ")
if project_exists(project) == False:
    print("Hm, that project doesn't exist. Should I create it? Yes/no: ")
    response = input(">> ")
    if response == "no":
        exit()
    print("Sounds good! Let's get to it.")
    make_project(project)

dbs = load_dbs(project)
client = dbs[0]
background = dbs[1]
ideas = dbs[2]

query = input(">> ")
context = deque(maxlen=15)
context.append(query)

while(query != "EXIT"): 
    if query == "INGEST":
        print("Ingesting new information...")
        ingest(project,background)
        print("done")
    elif query == "IDEA":
        print("Ready for your idea: ")
        new_idea = input(">> ")
        record_idea(new_idea,ideas)
    elif query == "CLEAR":
        context.clear()
    else: #not a system command, so a query
        print("Summarizing!")
        #llm.system_prompt = summary_prompt
        summary = llm.query(" ".join(context))
        print("Done!")
        llm.system_prompt = query_prompt
    
        #print(background.peek())
        
        #print(query)

        #top_n_result = chroma_utils.get_top_N_from_chroma(background, query, 1)
        #print(type(top_n_result), top_n_result)
        
        embed = embModel.encode(query).tolist()
        #bg = " ".join(chroma_utils.get_top_N_from_chroma(background, query, 5))
        bg = " ".join(chroma_utils.get_top_N_from_chroma(background, embed, 5))

        #print("bg is: " + bg)

        llm_query = "The background information that may be relevant is: " + bg + "and for this one, let's use only the background and not any previous ideas. The user query is: " + query
        output = llm.query("<SYS_PROMPT>" + query_prompt + "</SYS_PROMPT>" + llm_query)
        # Adding the timestamp
        local_tz = pytz.timezone('America/Toronto')
        current_time_utc = datetime.now(pytz.utc)
        current_time_local = current_time_utc.astimezone(local_tz)
        formatted_time = current_time_local.strftime("====Timestamp: %Y-%m-%d %H:%M:%S.%f%z")
        print("==============================================================================")
        print("====LLM: " + llmName)
        print("====Embedding: " + embName)
        print(formatted_time)
        print("====Query: " + query )
        print("====Context: " + bg)
        print("====Output: " + output[1])
        print("==============================================================================")
        print("\n")
        print("\n")
             
    query = input(">> ")

